var React = require('react');
var ReactDOM = require('react-dom');
var List = require('./components/List');
var {Provider} = require('react-redux');
var store = require('store');

ReactDOM.render(
  <Provider store={store}>
    <List/>
  </Provider>,    
  document.getElementById('root')
);


// var obj = {
//   name: 'as',
//   age: 18
// }

// var obj2 = {...obj};
// obj.age = 30;
// console.log(obj2);

// var mang = [5,3,8,5];
// var arr = [...mang];
// console.log(mang);

// var add = (a,b) => a + b;
// console.log(add(5,6))

// require('./examples');